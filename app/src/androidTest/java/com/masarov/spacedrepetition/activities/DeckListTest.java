package com.masarov.spacedrepetition.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import com.masarov.spacedrepetition.R;
import com.masarov.spacedrepetition.di.TestHelper;
import com.masarov.spacedrepetition.model.Deck;
import com.masarov.spacedrepetition.storage.DeckRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class DeckListTest {

  @Rule
  public ActivityTestRule<DeckListActivity> activityRule = new ActivityTestRule<>(DeckListActivity.class, true, false);

  @Inject
  DeckRepository deckRepository;

  @Before
  public void setUp() throws Exception {
    TestHelper.getTestComponent().inject(this);
  }

  @Test
  public void testEmptyLaunchShowsMessage() {
    when(deckRepository.getAll()).thenReturn(new ArrayList<Deck>());

    startActivity();

    onView(withText(R.string.empty_text)).check(matches(isDisplayed()));
    verify(deckRepository).getAll();
  }

  private void startActivity() {
    activityRule.launchActivity(new Intent());
  }

  @Test
  public void testDeckNamesAreShown() throws Exception {
    when(deckRepository.getAll()).thenReturn(createValidDecks());

    startActivity();

    onView(withText("English II")).check(matches(isDisplayed()));
    onView(withText("German II")).check(matches(isDisplayed()));
    verify(deckRepository).getAll();
  }

  @Test
  public void testDeckNumberOfCardsAreShown() throws Exception {
    when(deckRepository.getAll()).thenReturn(createValidDecks());

    startActivity();

    onView(withText("35 cards")).check(matches(isDisplayed()));
    onView(withText("12 cards")).check(matches(isDisplayed()));
    verify(deckRepository).getAll();
  }

  @Test
  public void testDeckDatesAreShown() throws Exception {
    when(deckRepository.getAll()).thenReturn(createValidDecks());

    startActivity();

    onView(withText("08.10.2015")).check(matches(isDisplayed()));
    onView(withText("12.11.2015")).check(matches(isDisplayed()));
    verify(deckRepository).getAll();
  }

  @NonNull
  private List<Deck> createValidDecks() {
    List<Deck> decks = new ArrayList<>(2);
    decks.add(new Deck(1444313121493L, "English II", "url", 35));
    decks.add(new Deck(1447316121493L, "German II", "url", 12));
    return decks;
  }

}