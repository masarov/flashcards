package com.masarov.spacedrepetition.activities;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import com.masarov.spacedrepetition.R;
import com.masarov.spacedrepetition.di.TestHelper;
import com.masarov.spacedrepetition.storage.DeckRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class AddDeckTest {

  private static final String VALID_DECK_NAME = "DeckName";

  @Rule
  public ActivityTestRule<EditDeckActivity> activityRule = new ActivityTestRule<>(EditDeckActivity.class, true, false);

  @Inject
  DeckRepository deckRepository;

  @Before
  public void setUp() throws Exception {
    TestHelper.getTestComponent().inject(this);
    when(deckRepository.exists(anyString())).thenReturn(false);
  }

  @Test
  public void testEmptyDeckAndUrlFieldsErrorIsShown() {
    activityRule.launchActivity(new Intent());
    onView(withId(R.id.download_deck_button)).perform(click());

    onView(withText(R.string.url_empty)).check(matches(isDisplayed()));
    onView(withText(R.string.name_empty)).check(matches(isDisplayed()));
  }

  @Test
  public void testEmptySpaceDeckNameErrorIsShown() {
    activityRule.launchActivity(new Intent());
    onView(withId(R.id.deck_name_field)).perform(typeText("     "));

    onView(withId(R.id.download_deck_button)).perform(click());

    onView(withText(R.string.url_empty)).check(matches(isDisplayed()));
    onView(withText(R.string.name_empty)).check(matches(isDisplayed()));
  }

  @Test
  public void testDeckNameAlreadyExistsErrorIsShown() {
    when(deckRepository.exists(anyString())).thenReturn(true);
    activityRule.launchActivity(new Intent());
    onView(withId(R.id.deck_name_field)).perform(typeText("Existing name"));

    onView(withId(R.id.download_deck_button)).perform(click());

    onView(withText(R.string.url_empty)).check(matches(isDisplayed()));
    onView(withText(R.string.name_not_unique)).check(matches(isDisplayed()));
  }

  @Test
  public void testEmptyURLErrorIsShown() {
    activityRule.launchActivity(new Intent());
    onView(withId(R.id.deck_name_field)).perform(typeText(VALID_DECK_NAME));

    onView(withId(R.id.download_deck_button)).perform(click());

    onView(withText(R.string.url_empty)).check(matches(isDisplayed()));
  }

  @Test
  public void testEmptySpaceURLErrorIsShown() {
    activityRule.launchActivity(new Intent());
    onView(withId(R.id.deck_name_field)).perform(typeText(VALID_DECK_NAME));
    onView(withId(R.id.deck_url_field)).perform(typeText("         "));

    onView(withId(R.id.download_deck_button)).perform(click());

    onView(withText(R.string.url_empty)).check(matches(isDisplayed()));
  }

  @Test
  public void testURLNotGoogleErrorIsShown() {
    activityRule.launchActivity(new Intent());
    onView(withId(R.id.deck_name_field)).perform(typeText(VALID_DECK_NAME));
    onView(withId(R.id.deck_url_field)).perform(typeText("https://www.ttu.ee/something"));

    onView(withId(R.id.download_deck_button)).perform(click());

    onView(withText(R.string.url_not_google)).check(matches(isDisplayed()));
  }

  @Test
  public void testURLNotSpreadsheetErrorIsShown() {
    activityRule.launchActivity(new Intent());
    onView(withId(R.id.deck_name_field)).perform(typeText(VALID_DECK_NAME));
    onView(withId(R.id.deck_url_field)).perform(typeText("https://docs.google.com/drawing/"));

    onView(withId(R.id.download_deck_button)).perform(click());

    onView(withText(R.string.url_not_spreadsheet)).check(matches(isDisplayed()));
  }

  @Test
  public void testURLNotHttpErrorIsShown() {
    activityRule.launchActivity(new Intent());
    onView(withId(R.id.deck_name_field)).perform(typeText(VALID_DECK_NAME));
    onView(withId(R.id.deck_url_field)).perform(typeText("ftp://docs.google.com/spreadsheets/"));

    onView(withId(R.id.download_deck_button)).perform(click());

    onView(withText(R.string.url_not_https)).check(matches(isDisplayed()));
  }

  @Test
  public void testUrlNotPublicErrorIsShown() {
    activityRule.launchActivity(new Intent());
    onView(withId(R.id.deck_name_field)).perform(typeText(VALID_DECK_NAME));
    onView(withId(R.id.deck_url_field)).perform(typeText("https://docs.google.com/spreadsheets/d/16EY3s3nATFN/output=csv"));

    onView(withId(R.id.download_deck_button)).perform(click());

    onView(withText(R.string.url_not_public)).check(matches(isDisplayed()));
  }

  @Test
  public void testUrlWrongOutputFormatErrorIsShown() {
    activityRule.launchActivity(new Intent());
    onView(withId(R.id.deck_name_field)).perform(typeText(VALID_DECK_NAME));
    onView(withId(R.id.deck_url_field)).perform(typeText("https://docs.google.com/spreadsheets/d/16EY3s3nATFNOEXr/pub?output=json"));

    onView(withId(R.id.download_deck_button)).perform(click());

    onView(withText(R.string.url_wrong_output)).check(matches(isDisplayed()));
  }

}
