package com.masarov.spacedrepetition.activities;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import com.masarov.spacedrepetition.R;
import com.masarov.spacedrepetition.storage.CardRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static com.masarov.spacedrepetition.di.TestHelper.*;
import static org.hamcrest.core.AllOf.*;
import static org.mockito.Mockito.*;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class SessionTest {


  @Rule
  public ActivityTestRule<SessionActivity> activityRule = new ActivityTestRule<>(SessionActivity.class, true, false);

  @Inject
  CardRepository cardRepository;

  @Before
  public void setUp() throws Exception {
    getTestComponent().inject(this);
    when(cardRepository.getShuffle(TEST_DECK_ID)).thenReturn(createListOfCards());
  }

  @Test
  public void testThatAnswerIsShownAfterClick() throws Exception {
    activityRule.launchActivity(getIntentWithDeckExtra());

    onView(withText(FIRST_QUESTION)).perform(click());

    onView(withText(FIRST_ANSWER)).check(matches(isDisplayed()));
  }

  @Test
  public void testQuestionIsShownAfterClickingOnAnswer() throws Exception {
    activityRule.launchActivity(getIntentWithDeckExtra());

    onView(withText(FIRST_QUESTION)).perform(click());
    onView(withText(FIRST_ANSWER)).perform(click());

    onView(withText(FIRST_QUESTION)).check(matches(isDisplayed()));
  }

  @Test
  public void testNextQuestionIsShownAfterCorrectAnswer() throws Exception {
    activityRule.launchActivity(getIntentWithDeckExtra());

    onView(withText(FIRST_QUESTION)).perform(click());
    onView(allOf(withId(R.id.correct), isDisplayed())).perform(click());

    onView(withText(SECOND_QUESTION)).check(matches(isDisplayed()));
  }

  @Test
  public void testNextQuestionIsShownAfterWrongAnswer() throws Exception {
    activityRule.launchActivity(getIntentWithDeckExtra());

    onView(withText(FIRST_QUESTION)).perform(click());
    onView(allOf(withId(R.id.incorrect), isDisplayed())).perform(click());

    onView(withText(SECOND_QUESTION)).check(matches(isDisplayed()));
  }

  @Test
  public void testResultIsShown() throws Exception {
    activityRule.launchActivity(getIntentWithDeckExtra());

    onView(withText(FIRST_QUESTION)).perform(click());
    onView(allOf(withId(R.id.incorrect), isDisplayed())).perform(click());
    onView(withText(SECOND_QUESTION)).perform(click());
    onView(allOf(withId(R.id.correct), isDisplayed())).perform(click());

    onView(withId(R.id.correct_answers_view)).check(matches(withText(SECOND_ANSWER + "\n")));
    onView(withId(R.id.wrong_answers_view)).check(matches(withText(FIRST_ANSWER + "\n")));
  }

}
