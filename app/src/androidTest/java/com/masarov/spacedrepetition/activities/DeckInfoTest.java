package com.masarov.spacedrepetition.activities;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import com.masarov.spacedrepetition.R;
import com.masarov.spacedrepetition.di.TestHelper;
import com.masarov.spacedrepetition.storage.CardRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static com.masarov.spacedrepetition.di.TestHelper.*;
import static org.mockito.Mockito.*;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class DeckInfoTest {

  @Rule
  public ActivityTestRule<DeckInfoActivity> activityRule = new ActivityTestRule<>(DeckInfoActivity.class, true, false);

  @Inject
  CardRepository cardRepository;

  @Before
  public void setUp() throws Exception {
    TestHelper.getTestComponent().inject(this);
  }

  @Test
  public void testEditButtonIsDisplayed() throws Exception {
    activityRule.launchActivity(getIntentWithDeckExtra());

    onView(withId(R.id.edit_deck_button)).check(matches(isDisplayed()));
  }

  @Test
  public void testStartSessionButtonIsDisplayed() throws Exception {
    activityRule.launchActivity(getIntentWithDeckExtra());

    onView(withId(R.id.start_session_fab)).check(matches(isDisplayed()));
  }

  @Test
  public void testCardsAreShown() throws Exception {
    when(cardRepository.get(TEST_DECK_ID)).thenReturn(createListOfCards());
    activityRule.launchActivity(getIntentWithDeckExtra());

    onView(withId(R.id.cards_view)).check(matches(isDisplayed()));
    onView(withId(R.id.cards_view)).check(matches(withText("Question - 1 - Answer - 1\nQuestion - 2 - Answer - 2\n")));
  }
}
