package com.masarov.spacedrepetition.activities;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import com.masarov.spacedrepetition.R;
import com.masarov.spacedrepetition.di.TestHelper;
import com.masarov.spacedrepetition.model.Deck;
import com.masarov.spacedrepetition.storage.DeckRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static com.masarov.spacedrepetition.di.TestHelper.*;
import static org.mockito.Mockito.*;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class EditDeckTest {

  @Rule
  public ActivityTestRule<EditDeckActivity> activityRule = new ActivityTestRule<>(EditDeckActivity.class, true, false);

  @Inject
  DeckRepository deckRepository;

  @Before
  public void setUp() throws Exception {
    TestHelper.getTestComponent().inject(this);
  }

  @Test
  public void testNameAndUrlFieldsAreFilled() throws Exception {
    activityRule.launchActivity(getIntentWithDeckExtra());

    onView(withId(R.id.deck_name_field)).check(matches(withText(TEST_DECK_NAME)));
    onView(withId(R.id.deck_url_field)).check(matches(withText(TEST_DECK_URL)));
  }

  @Test
  public void testEditButtonsAreDisplayed() throws Exception {
    activityRule.launchActivity(getIntentWithDeckExtra());

    onView(withId(R.id.delete_deck_button)).check(matches(isDisplayed()));
    onView(withId(R.id.save_deck_button)).check(matches(isDisplayed()));
  }

  @Test
  public void testChangedDeckIsSaved() {
    activityRule.launchActivity(getIntentWithDeckExtra());

    onView(withId(R.id.save_deck_button)).perform(click());

    verify(deckRepository).save(any(Deck.class));
  }

  @Test
  public void testDeleteDialogIsDisplayed() throws Exception {
    activityRule.launchActivity(getIntentWithDeckExtra());

    onView(withId(R.id.delete_deck_button)).perform(click());

    onView(withText(R.string.dialog_delete_header)).check(matches(isDisplayed()));
    onView(withText(R.string.dialog_delete_message)).check(matches(isDisplayed()));
  }

  @Test
  public void testDeleteDialogIsCancelled() throws Exception {
    activityRule.launchActivity(getIntentWithDeckExtra());

    onView(withId(R.id.delete_deck_button)).perform(click());

    onView(withText(R.string.dialog_delete_negative_action)).perform(click());
    verifyZeroInteractions(deckRepository);
  }

  @Test
  public void testDeleteDialogIsAccepted() throws Exception {
    activityRule.launchActivity(getIntentWithDeckExtra());

    onView(withId(R.id.delete_deck_button)).perform(click());

    onView(withText(R.string.dialog_delete_positive_action)).perform(click());
    verify(deckRepository).delete(TEST_DECK_ID);
  }

}
