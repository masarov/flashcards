package com.masarov.spacedrepetition.di;

import com.masarov.spacedrepetition.activities.AddDeckTest;
import com.masarov.spacedrepetition.activities.DeckInfoTest;
import com.masarov.spacedrepetition.activities.DeckListTest;
import com.masarov.spacedrepetition.activities.EditDeckTest;
import com.masarov.spacedrepetition.activities.SessionTest;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = MockApplicationModule.class)
public interface TestComponent extends ApplicationComponent {
  void inject(DeckListTest test);

  void inject(EditDeckTest test);

  void inject(AddDeckTest test);

  void inject(SessionTest test);

  void inject(DeckInfoTest test);
}
