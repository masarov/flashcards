package com.masarov.spacedrepetition.di;

import com.masarov.spacedrepetition.storage.CardRepository;
import com.masarov.spacedrepetition.storage.DeckRepository;
import com.masarov.spacedrepetition.storage.ResultRepository;
import dagger.Module;
import dagger.Provides;
import org.mockito.Mockito;

import javax.inject.Singleton;

@Module
public class MockApplicationModule {

  @Singleton
  @Provides
  public CardRepository provideCardRepository() {
    return Mockito.mock(CardRepository.class);
  }

  @Singleton
  @Provides
  public DeckRepository provideDeckRepository() {
    return Mockito.mock(DeckRepository.class);
  }

  @Singleton
  @Provides
  public ResultRepository provideResultRepository() {
    return Mockito.mock(ResultRepository.class);
  }
}
