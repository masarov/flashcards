package com.masarov.spacedrepetition.di;


import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import com.masarov.spacedrepetition.model.Card;
import com.masarov.spacedrepetition.model.Deck;

import java.util.ArrayList;
import java.util.List;

public class TestHelper {

  public static final int TEST_DECK_ID = 999;
  public static final String TEST_DECK_NAME = "German II";
  public static final String TEST_DECK_URL = "https://docs.google.com/spreadsheets/d/1zYI/pub?output=csv";

  public static final String FIRST_QUESTION = "Question - 1";
  public static final String FIRST_ANSWER = "Answer - 1";
  public static final String SECOND_QUESTION = "Question - 2";
  public static final String SECOND_ANSWER = "Answer - 2";

  public static TestComponent getTestComponent() {
    Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
    MainApplication application = (MainApplication)instrumentation.getTargetContext().getApplicationContext();
    TestComponent component = DaggerTestComponent.create();
    application.setComponent(component);
    return component;
  }

  public static Intent getIntentWithDeckExtra() {
    Intent intent = new Intent();
    intent.putExtra(Deck.EXTRA, new Deck(TEST_DECK_ID, TEST_DECK_NAME, TEST_DECK_URL, 0));
    return intent;
  }

  public static List<Card> createListOfCards() {
    List<Card> list = new ArrayList<>(3);
    list.add(new Card(FIRST_QUESTION, FIRST_ANSWER));
    list.add(new Card(SECOND_QUESTION, SECOND_ANSWER));
    return list;
  }
}
