package com.masarov.spacedrepetition.model;


import android.os.Parcel;
import android.os.Parcelable;

public class Deck implements Parcelable {

  public static final String EXTRA = "DeckExtra";

  private long id;
  private String name;
  private String url;
  private int numberOfCards;

  protected Deck(Parcel in) {
    id = in.readLong();
    name = in.readString();
    url = in.readString();
    numberOfCards = in.readInt();
  }

  public Deck(long id, String name, String url, int numberOfCards) {
    this.id = id;
    this.name = name;
    this.url = url;
    this.numberOfCards = numberOfCards;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public int getNumberOfCards() {
    return numberOfCards;
  }

  public void setNumberOfCards(int numberOfCards) {
    this.numberOfCards = numberOfCards;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel out, int flags) {
    out.writeLong(id);
    out.writeString(name);
    out.writeString(url);
    out.writeInt(numberOfCards);
  }

  @SuppressWarnings("unused")
  public static final Parcelable.Creator<Deck> CREATOR = new Parcelable.Creator<Deck>() {
    @Override
    public Deck createFromParcel(Parcel in) {
      return new Deck(in);
    }

    @Override
    public Deck[] newArray(int size) {
      return new Deck[size];
    }
  };

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Deck deck = (Deck)o;

    if (getId() != deck.getId()) {
      return false;
    }
    if (getNumberOfCards() != deck.getNumberOfCards()) {
      return false;
    }
    if (getName() != null ? !getName().equals(deck.getName()) : deck.getName() != null) {
      return false;
    }
    return !(getUrl() != null ? !getUrl().equals(deck.getUrl()) : deck.getUrl() != null);

  }

  @Override
  public int hashCode() {
    int result = (int)(getId() ^ (getId() >>> 32));
    result = 31 * result + (getName() != null ? getName().hashCode() : 0);
    result = 31 * result + (getUrl() != null ? getUrl().hashCode() : 0);
    result = 31 * result + getNumberOfCards();
    return result;
  }
}