package com.masarov.spacedrepetition.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Session implements Parcelable {

  public static final String EXTRA = "SessionExtra";

  private List<Card> correctAnswers;
  private List<Card> wrongAnswers;

  public List<Card> getCorrectAnswers() {
    if (correctAnswers == null) {
      correctAnswers = new ArrayList<>();
    }
    return correctAnswers;
  }

  public List<Card> getWrongAnswers() {
    if (wrongAnswers == null) {
      wrongAnswers = new ArrayList<>();
    }
    return wrongAnswers;
  }

  public Session(){

  }

  protected Session(Parcel in) {
    if (in.readByte() == 0x01) {
      correctAnswers = new ArrayList<>();
      in.readList(correctAnswers, Card.class.getClassLoader());
    }
    else {
      correctAnswers = null;
    }
    if (in.readByte() == 0x01) {
      wrongAnswers = new ArrayList<>();
      in.readList(wrongAnswers, Card.class.getClassLoader());
    }
    else {
      wrongAnswers = null;
    }
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel out, int flags) {
    if (correctAnswers == null) {
      out.writeByte((byte)(0x00));
    }
    else {
      out.writeByte((byte)(0x01));
      out.writeList(correctAnswers);
    }
    if (wrongAnswers == null) {
      out.writeByte((byte)(0x00));
    }
    else {
      out.writeByte((byte)(0x01));
      out.writeList(wrongAnswers);
    }
  }

  @SuppressWarnings("unused")
  public static final Parcelable.Creator<Session> CREATOR = new Parcelable.Creator<Session>() {
    @Override
    public Session createFromParcel(Parcel in) {
      return new Session(in);
    }

    @Override
    public Session[] newArray(int size) {
      return new Session[size];
    }
  };
}