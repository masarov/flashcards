package com.masarov.spacedrepetition.model;

public enum Retention {
  NONE, ONE, THREE, TEN, THIRTY, SIXTY, ARCHIVE;

  public Retention incrementRetention(Retention retention) {
    switch (retention) {
      case NONE:
        return ONE;
      case ONE:
        return THREE;
      case THREE:
        return TEN;
      case TEN:
        return THIRTY;
      case THIRTY:
        return SIXTY;
      case SIXTY:
        return ARCHIVE;
      default:
        return ONE;
    }
  }
}
