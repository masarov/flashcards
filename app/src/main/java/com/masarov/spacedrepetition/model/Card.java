package com.masarov.spacedrepetition.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Card implements Parcelable {

  public static final String EXTRA = "CardExtra";

  private String question;
  private String answer;

  public Card(String question, String answer) {
    this.question = question;
    this.answer = answer;
  }

  protected Card(Parcel in) {
    question = in.readString();
    answer = in.readString();
  }

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }

  public String getAnswer() {
    return answer;
  }

  public void setAnswer(String answer) {
    this.answer = answer;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel out, int flags) {
    out.writeString(question);
    out.writeString(answer);
  }

  @SuppressWarnings("unused")
  public static final Parcelable.Creator<Card> CREATOR = new Parcelable.Creator<Card>() {
    @Override
    public Card createFromParcel(Parcel in) {
      return new Card(in);
    }

    @Override
    public Card[] newArray(int size) {
      return new Card[size];
    }
  };

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Card card = (Card)o;

    if (getQuestion() != null ? !getQuestion().equals(card.getQuestion()) : card.getQuestion() != null) {
      return false;
    }
    return !(getAnswer() != null ? !getAnswer().equals(card.getAnswer()) : card.getAnswer() != null);

  }

  @Override
  public int hashCode() {
    int result = getQuestion() != null ? getQuestion().hashCode() : 0;
    result = 31 * result + (getAnswer() != null ? getAnswer().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return question + " - " + answer;
  }
}