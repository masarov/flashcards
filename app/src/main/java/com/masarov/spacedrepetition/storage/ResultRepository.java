package com.masarov.spacedrepetition.storage;


import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.masarov.spacedrepetition.utils.PreferenceUtils;

public class ResultRepository {

  private SharedPreferences preferences;
  private Gson gson;

  public ResultRepository(SharedPreferences preferences, Gson gson) {
    this.preferences = preferences;
    this.gson = gson;
  }

  public void log() {
    PreferenceUtils.logVeryLongString(preferences.getAll().toString());
  }
}
