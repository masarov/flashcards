package com.masarov.spacedrepetition.storage;


import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.masarov.spacedrepetition.model.Card;
import com.masarov.spacedrepetition.utils.PreferenceUtils;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import static com.masarov.spacedrepetition.utils.PreferenceUtils.*;

public class CardRepository {

  private static final Type CARDS_TYPE = new TypeToken<List<Card>>() {}.getType();

  private SharedPreferences preferences;
  private Gson gson;

  public CardRepository(SharedPreferences preferences, Gson gson) {
    this.preferences = preferences;
    this.gson = gson;
  }

  @NonNull
  public List<Card> get(long deckId) {
    validate(deckId);
    return gson.fromJson(preferences.getString(Long.toString(deckId), EMPTY_LIST_STRING), CARDS_TYPE);
  }

  @NonNull
  public List<Card> getShuffle(long deckId) {
    List<Card> cards = get(deckId);
    Collections.shuffle(cards);
    return cards;
  }

  public void save(List<Card> cards, long deckId) {
    validate(deckId);
    preferences.edit().putString(Long.toString(deckId), gson.toJson(cards, CARDS_TYPE)).apply();
  }

  public void delete(long deckId) {
    validate(deckId);
    preferences.edit().remove(Long.toString(deckId)).apply();
  }

  public void log() {
    PreferenceUtils.logVeryLongString(preferences.getAll().toString());
  }

  public void clear() {
    preferences.edit().clear().apply();
  }

  private void validate(long deckId) {
    if (deckId == 0 || deckId == -1L) {
      throw new IllegalArgumentException("Deck id should be valid. Was " + deckId);
    }
  }
}
