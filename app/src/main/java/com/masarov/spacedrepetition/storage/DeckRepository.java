package com.masarov.spacedrepetition.storage;


import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.masarov.spacedrepetition.model.Deck;
import com.masarov.spacedrepetition.utils.PreferenceUtils;

import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.List;

import static com.masarov.spacedrepetition.utils.PreferenceUtils.*;

public class DeckRepository {

  private static final String DECKS_KEY = "DeckRepository";
  private static final Type DECK_TYPE = new TypeToken<List<Deck>>() {}.getType();

  private SharedPreferences preferences;
  private Gson gson;

  public DeckRepository(SharedPreferences preferences, Gson gson) {
    this.preferences = preferences;
    this.gson = gson;
  }

  @NonNull
  public List<Deck> getAll() {
    return gson.fromJson(preferences.getString(DECKS_KEY, EMPTY_LIST_STRING), DECK_TYPE);
  }

  @NonNull
  public Deck get(long deckId) {
    validate(deckId);

    List<Deck> decks = getAll();
    for (Deck deck : decks) {
      if (deck.getId() == deckId) {
        return deck;
      }
    }

    throw new IllegalArgumentException("Trying to get non-existent deck with id " + deckId);
  }

  public void add(Deck deck) {
    validate(deck);
    List<Deck> decks = getAll();
    decks.add(deck);
    save(decks);
  }

  public void delete(long deckId) {
    validate(deckId);

    List<Deck> decks = getAll();
    Iterator<Deck> iterator = decks.iterator();
    while (iterator.hasNext()) {
      Deck deck = iterator.next();
      if (deck.getId() == deckId) {
        iterator.remove();
      }
    }

    save(decks);
  }

  public void save(Deck changedDeck) {
    validate(changedDeck);

    List<Deck> decks = getAll();
    for (Deck deck : decks) {
      if (deck.getId() == changedDeck.getId()) {
        deck.setName(changedDeck.getName());
        deck.setUrl(changedDeck.getUrl());
        deck.setNumberOfCards(changedDeck.getNumberOfCards());
      }
    }
    save(decks);
  }

  private void save(List<Deck> decks) {
    preferences.edit().putString(DECKS_KEY, gson.toJson(decks, DECK_TYPE)).apply();
  }

  public boolean exists(String deckName) {
    if (deckName == null || deckName.trim().isEmpty()) {
      throw new IllegalArgumentException("Deck name should be not null or empty.");
    }

    List<Deck> decks = getAll();
    for (Deck deck : decks) {
      if (deck.getName().equals(deckName)) {
        return true;
      }
    }
    return false;
  }

  public void log() {
    PreferenceUtils.logVeryLongString(preferences.getAll().toString());
  }

  public void clear() {
    preferences.edit().clear().apply();
  }

  private void validate(Deck deck) {
    if (deck == null) {
      throw new IllegalArgumentException("Deck should not be null");
    }

    if (deck.getName() == null || deck.getUrl() == null || deck.getName().trim().isEmpty() || deck.getUrl().trim().isEmpty()) {
      throw new IllegalArgumentException("Deck name and url cannot be null or empty.");
    }

    validate(deck.getId());
  }

  private void validate(long deckId) {
    if (deckId == 0L || deckId == -1L) {
      throw new IllegalArgumentException("Deck id should be valid. Was " + deckId);
    }
  }

}
