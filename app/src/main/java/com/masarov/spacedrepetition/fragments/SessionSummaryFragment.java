package com.masarov.spacedrepetition.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.masarov.spacedrepetition.R;
import com.masarov.spacedrepetition.model.Card;
import com.masarov.spacedrepetition.model.Session;

import java.util.List;

public class SessionSummaryFragment extends Fragment implements OnSummaryReadyListener {

  @Bind(R.id.correct_answers_view)
  TextView correctAnswersView;

  @Bind(R.id.wrong_answers_view)
  TextView wrongAnswersView;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_session_summary, container, false);
    ButterKnife.bind(this, view);

    return view;
  }

  private void setResult(List<Card> cards, TextView resultView) {
    StringBuilder wordsString = new StringBuilder(cards.size() * 15);
    for (Card card : cards) {
      wordsString.append(card.getAnswer()).append("\n");
    }
    resultView.setText(wordsString.length() > 0 ? wordsString.toString() : getString(R.string.nothing));
  }

  @OnClick(R.id.ok_button)
  public void goBack() {
    Intent upIntent = NavUtils.getParentActivityIntent(getActivity());
    if (NavUtils.shouldUpRecreateTask(getActivity(), upIntent)) {
      TaskStackBuilder.create(getActivity())
              .addNextIntentWithParentStack(upIntent)
              .startActivities();
    }
    else {
      NavUtils.navigateUpTo(getActivity(), upIntent);
    }
  }

  @Override
  public void onSummaryReady(Session session) {
    setResult(session.getCorrectAnswers(), correctAnswersView);
    setResult(session.getWrongAnswers(), wrongAnswersView);
  }
}
