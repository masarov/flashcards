package com.masarov.spacedrepetition.fragments;


import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.masarov.spacedrepetition.R;
import com.masarov.spacedrepetition.utils.ConversionResult;

public class DownloadResultFragment extends Fragment {

  public static final String RESULT = "Result";

  @Bind(R.id.correct_answers_view)
  TextView warningsView;

  @Bind(R.id.errors_view)
  TextView errorsView;

  @Bind(R.id.cards_view)
  TextView cardsView;

  @Bind(R.id.result_image_view)
  ImageView resultImageView;

  @Bind(R.id.warnings_heading)
  View warningsHeading;

  @Bind(R.id.saved_cards_heading)
  View savedCardsHeading;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_download_result, container, false);
    ButterKnife.bind(this, view);

    ConversionResult result = getArguments().getParcelable(RESULT);
    if (result == null) {
      throw new IllegalArgumentException(getString(R.string.result_null));
    }

    initViews(result);
    return view;
  }

  private void initViews(ConversionResult result) {
    resultImageView.setImageDrawable(getResultDrawable(result));
    resultImageView.setColorFilter(getResultColor(result));

    if (result.getError() != null) {
      prepareErrorView(result);
    }
    if (!result.getWarnings().isEmpty()) {
      prepareWarningView(result);
    }
    prepareCardsView(result);
  }

  private Drawable getResultDrawable(ConversionResult result) {
    return getActivity().getResources().getDrawable(
            result.getError() != null ? R.drawable.ic_error_black_48dp :
            (result.getWarnings().isEmpty() ? R.drawable.ic_check_circle_black_48dp : R.drawable.ic_warning_black_48dp));
  }

  private int getResultColor(ConversionResult result) {
    return getActivity().getResources().getColor(
            result.getError() != null ? R.color.wrong_answer_color :
            (result.getWarnings().isEmpty() ? R.color.correct_answer_color : R.color.orange_a400));
  }

  private void prepareErrorView(ConversionResult result) {
    errorsView.setVisibility(View.VISIBLE);
    errorsView.setText(result.getError() == null ? "" : result.getError());
  }

  private void prepareWarningView(ConversionResult result) {
    warningsHeading.setVisibility(View.VISIBLE);
    warningsView.setVisibility(View.VISIBLE);
    warningsView.setText(TextUtils.join("\n", result.getWarnings()));
  }

  private void prepareCardsView(ConversionResult result) {
    cardsView.setVisibility(result.getCards().isEmpty() ? View.GONE : View.VISIBLE);
    savedCardsHeading.setVisibility(result.getCards().isEmpty() ? View.GONE : View.VISIBLE);
    cardsView.setText(TextUtils.join("\n", result.getCards()));
  }

  @OnClick(R.id.ok_button)
  public void goBack() {
    Intent upIntent = NavUtils.getParentActivityIntent(getActivity());
    if (NavUtils.shouldUpRecreateTask(getActivity(), upIntent)) {
      TaskStackBuilder.create(getActivity())
              .addNextIntentWithParentStack(upIntent)
              .startActivities();
    }
    else {
      NavUtils.navigateUpTo(getActivity(), upIntent);
    }
  }
}
