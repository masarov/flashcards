package com.masarov.spacedrepetition.fragments;


import com.masarov.spacedrepetition.model.Session;

public interface OnSummaryReadyListener {
  void onSummaryReady(Session session);
}
