package com.masarov.spacedrepetition.fragments;

import com.masarov.spacedrepetition.model.Session;


public interface SessionProvider {
  Session provideSession();
}
