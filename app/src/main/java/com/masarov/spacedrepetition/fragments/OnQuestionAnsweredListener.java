package com.masarov.spacedrepetition.fragments;


public interface OnQuestionAnsweredListener {
  void onQuestionAnswered(boolean correct);
}
