package com.masarov.spacedrepetition.fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.masarov.spacedrepetition.R;
import com.masarov.spacedrepetition.model.Deck;
import com.masarov.spacedrepetition.utils.UrlValidator;

public class EditDeckFragment extends Fragment {

  public static final String EXISTING_DECK = "ExistingDeck";
  public static final String DECK_URL = "DeckUrl";

  @Bind(R.id.deck_name_field)
  EditText deckNameField;

  @Bind(R.id.deck_url_field)
  EditText deckUrlField;

  @Bind(R.id.delete_deck_button)
  Button deleteDeckButton;

  @Bind(R.id.save_deck_button)
  Button saveDeckButton;

  @Bind(R.id.deck_name_input_layout)
  TextInputLayout deckNameInputLayout;

  @Bind(R.id.deck_url_input_layout)
  TextInputLayout deckUrlInputLayout;

  private OnDeckChangeListener listener;
  private Deck deck;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_edit_deck, container, false);
    ButterKnife.bind(this, view);
    deck = getArguments().getParcelable(EXISTING_DECK);
    setUpViews();
    return view;
  }

  private void setUpViews() {
    if (deck == null) {
      deckUrlField.setText(getArguments().getString(DECK_URL, ""));
      deckNameField.requestFocus();
    }
    else {
      fillExistingData();
    }
  }

  private void fillExistingData() {
    deckNameField.setText(deck.getName());
    deckUrlField.setText(deck.getUrl());
    saveDeckButton.setVisibility(View.VISIBLE);
    deleteDeckButton.setVisibility(View.VISIBLE);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    try {
      listener = (OnDeckChangeListener)context;
    }
    catch (ClassCastException e) {
      throw new ClassCastException(getString(R.string.on_deck_change));
    }
  }

  @OnClick(R.id.delete_deck_button)
  public void deleteDeck() {
    ensureDeck();
    showDeleteAlertDialog(deck.getId());
  }

  @OnClick(R.id.save_deck_button)
  public void changeDeck() {
    ensureDeck();
    String name = getCorrectName(false);
    String url = getCorrectUrl();
    if (name == null || url == null) {
      return;
    }

    deck.setName(name);
    deck.setUrl(url);
    listener.onChangeDeck(deck);
  }

  private void ensureDeck() {
    if (deck == null) {
      throw new UnsupportedOperationException(getString(R.string.deck_null));
    }
  }

  @OnClick(R.id.download_deck_button)
  public void startDownload() {
    String name = getCorrectName(true);
    String url = getCorrectUrl();
    if (name == null || url == null) {
      return;
    }

    listener.onStartDownload(name, url);
  }

  @Nullable
  private String getCorrectName(boolean forDownload) {
    String name = deckNameField.getText().toString().trim();
    String nameError = getNameValidationError(name, forDownload);
    deckNameInputLayout.setError(nameError);
    return nameError == null ? name : null;
  }

  @Nullable
  private String getCorrectUrl() {
    String url = deckUrlField.getText().toString().trim();
    String nameError = UrlValidator.getSpreadsheetUrlError(url, getActivity());
    deckUrlInputLayout.setError(nameError);
    return nameError == null ? url : null;
  }

  private String getNameValidationError(String name, boolean forDownload) {
    if (name == null || name.trim().isEmpty()) {
      return getString(R.string.name_empty);
    }

    if ((!forDownload || deck == null) && !listener.isDeckNameUnique(name)) {
      return getString(R.string.name_not_unique);
    }

    if (forDownload && (deck != null && !name.equals(deck.getName()))) {
      return getString(R.string.name_not_same);
    }

    return null;
  }

  private void showDeleteAlertDialog(final long deckId) {
    new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme)
            .setTitle(getString(R.string.dialog_delete_header))
            .setMessage(getString(R.string.dialog_delete_message))
            .setPositiveButton(getString(R.string.dialog_delete_positive_action), new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                listener.onDeleteDeck(deckId);
              }
            })
            .setNegativeButton(getString(R.string.dialog_delete_negative_action), new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
              }
            }).show();
  }
}
