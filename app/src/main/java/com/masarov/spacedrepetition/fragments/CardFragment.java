package com.masarov.spacedrepetition.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.masarov.spacedrepetition.R;
import com.masarov.spacedrepetition.model.Card;
import com.masarov.spacedrepetition.ui.FlipAnimation;
import com.masarov.spacedrepetition.ui.HideViewOnAnimationEndListener;


public class CardFragment extends Fragment {

  private static final FastOutSlowInInterpolator FAST_OUT_SLOW_IN_INTERPOLATOR = new FastOutSlowInInterpolator();

  @Bind(R.id.question)
  TextView questionView;

  @Bind(R.id.answer)
  TextView answerView;

  @Bind(R.id.face_card_view)
  CardView faceCardView;

  @Bind(R.id.back_card_view)
  CardView backCardView;

  @Bind(R.id.buttons_card)
  CardView buttonsPanelView;

  @Bind(R.id.root_layout)
  RelativeLayout cardsLayout;

  private OnQuestionAnsweredListener listener;
  private Card card;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_view_card, container, false);
    ButterKnife.bind(this, view);
    extractCardExtra();
    initViews();
    return view;
  }

  private void extractCardExtra() {
    card = getArguments().getParcelable(Card.EXTRA);
    if (card == null) {
      throw new UnsupportedOperationException(getString(R.string.card_null));
    }
  }

  private void initViews() {
    questionView.setText(card.getQuestion());
    answerView.setText(card.getAnswer());
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    try {
      listener = (OnQuestionAnsweredListener)context;
    }
    catch (ClassCastException e) {
      throw new ClassCastException(getString(R.string.on_question_answered));
    }
  }

  @OnClick(R.id.face_card_view)
  public void handleFaceCardClick() {
    cardsLayout.startAnimation(new FlipAnimation(faceCardView, backCardView));
    buttonsPanelView.setAlpha(0f);
    buttonsPanelView.setVisibility(View.VISIBLE);
    buttonsPanelView.animate().alpha(1f).setDuration(500)
            .setInterpolator(FAST_OUT_SLOW_IN_INTERPOLATOR)
            .setListener(null);
  }

  @OnClick(R.id.answer)
  public void handleBackCardClick() {
    cardsLayout.startAnimation(new FlipAnimation(backCardView, faceCardView));
    buttonsPanelView.animate().alpha(0f).setDuration(500)
            .setInterpolator(FAST_OUT_SLOW_IN_INTERPOLATOR)
            .setListener(new HideViewOnAnimationEndListener(buttonsPanelView));
  }

  @OnClick(R.id.incorrect)
  public void handleIncorrectAnswer() {
    listener.onQuestionAnswered(false);
  }

  @OnClick(R.id.correct)
  public void handleCorrectAnswer() {
    listener.onQuestionAnswered(true);
  }
}
