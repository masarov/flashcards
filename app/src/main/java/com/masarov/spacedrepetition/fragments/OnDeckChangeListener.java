package com.masarov.spacedrepetition.fragments;

import com.masarov.spacedrepetition.model.Deck;

public interface OnDeckChangeListener {
  void onStartDownload(String deckName, String deckUrl);

  void onDeleteDeck(long deckId);

  void onChangeDeck(Deck deck);

  boolean isDeckNameUnique(String name);
}
