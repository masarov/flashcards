package com.masarov.spacedrepetition.utils;


import com.masarov.spacedrepetition.model.Card;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Converter {

  public static final String EMPTY_SPREADSHEET_ERROR = "Emtpy spreadsheet";
  public static final String WRONG_OUTPUT_FORMAT = "Wrong output format";

  private static final String LINE_DIVIDER = "\r\n";
  private static final String DOCTYPE_HTML = "<!DOCTYPE html>";

  public static ConversionResult convert(String url, String text) {
    ConversionResult result = new ConversionResult();

    if (text == null || text.trim().isEmpty()) {
      result.setError(EMPTY_SPREADSHEET_ERROR);
      return result;
    }

    if (text.startsWith(DOCTYPE_HTML)) {
      result.setError(WRONG_OUTPUT_FORMAT);
      return result;
    }

    ExtractionResult extractionResult = extractCards(text, UrlValidator.getSeparator(url));
    result.getWarnings().addAll(extractionResult.getCorruptLines());
    result.getCards().addAll(extractionResult.getCorrectCards());

    return result;
  }

  private static ExtractionResult extractCards(String csv, String divider) {
    ExtractionResult result = new ExtractionResult();
    String[] lines = csv.split(LINE_DIVIDER);

    for (String line : lines) {
      String[] parts = line.split(divider);

      if (parts.length != 2) {
        result.getCorruptLines().add(Arrays.toString(parts));
        continue;
      }

      result.getCorrectCards().add(new Card(parts[0].trim(), parts[1].trim()));
    }
    return result;
  }

  private static class ExtractionResult {
    private List<Card> correctCards;
    private List<String> corruptLines;

    public List<Card> getCorrectCards() {
      if (correctCards == null) {
        correctCards = new ArrayList<>();
      }
      return correctCards;
    }

    public List<String> getCorruptLines() {
      if (corruptLines == null) {
        corruptLines = new ArrayList<>();
      }
      return corruptLines;
    }
  }
}
