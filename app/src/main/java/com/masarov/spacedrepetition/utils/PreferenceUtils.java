package com.masarov.spacedrepetition.utils;


import android.util.Log;

public class PreferenceUtils {

  public static final String EMPTY_LIST_STRING = "[]";

  private static final String TAG = "SpacedRepetition";
  private static final int FOUR_THOUSAND = 4000;

  public static void logVeryLongString(String longString) {
    if (longString == null) {
      return;
    }

    if (longString.length() > FOUR_THOUSAND) {
      int chunkCount = longString.length() / FOUR_THOUSAND;
      for (int i = 0; i <= chunkCount; i++) {
        int max = FOUR_THOUSAND * (i + 1);
        if (max >= longString.length()) {
          Log.i(TAG, longString.substring(FOUR_THOUSAND * i));
        }
        else {
          Log.i(TAG, longString.substring(FOUR_THOUSAND * i, max));
        }
      }
    }
    else {
      Log.i(TAG, longString);
    }
  }

}
