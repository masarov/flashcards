package com.masarov.spacedrepetition.utils;


import android.content.Context;
import android.support.annotation.Nullable;
import com.masarov.spacedrepetition.R;

public class UrlValidator {

  private static final String DOCS_GOOGLE = "docs.google.com/";
  private static final String SPREADSHEETS = "spreadsheets/";
  private static final String HTTP_PREFIX = "http://";
  private static final String HTTPS_PREFIX = "https://";
  private static final String PUBLIC_POSTFIX = "/pub";
  private static final String OUTPUT_CSV = "output=csv";
  private static final String OUTPUT_TSV = "output=tsv";
  private static final String COMMA_SEPARATOR = ",";
  private static final String TAB_SEPARATOR = "\t";


  @Nullable
  public static String getSpreadsheetUrlError(String url, Context context) {
    if (url == null || url.trim().isEmpty()) {
      return context.getString(R.string.url_empty);
    }

    if (!url.contains(DOCS_GOOGLE)) {
      return context.getString(R.string.url_not_google);
    }

    if (!url.contains(SPREADSHEETS)) {
      return context.getString(R.string.url_not_spreadsheet);
    }

    if (!(url.startsWith(HTTP_PREFIX) || url.startsWith(HTTPS_PREFIX))) {
      return context.getString(R.string.url_not_https);
    }

    if (!url.contains(PUBLIC_POSTFIX)) {
      return context.getString(R.string.url_not_public);
    }

    if (!(url.contains(OUTPUT_CSV) || url.contains(OUTPUT_TSV))) {
      return context.getString(R.string.url_wrong_output);
    }

    return null;
  }

  public static String getSeparator(String url) {
    if (url != null && url.contains(OUTPUT_TSV)) {
      return TAB_SEPARATOR;
    }

    return COMMA_SEPARATOR;
  }

}
