package com.masarov.spacedrepetition.utils;

import android.os.Parcel;
import android.os.Parcelable;
import com.masarov.spacedrepetition.model.Card;

import java.util.ArrayList;
import java.util.List;

public class ConversionResult implements Parcelable {

  private List<Card> cards;
  private List<String> warnings;
  private String error;

  public ConversionResult(){

  }

  protected ConversionResult(Parcel in) {
    if (in.readByte() == 0x01) {
      cards = new ArrayList<>();
      in.readList(cards, Card.class.getClassLoader());
    }
    else {
      cards = null;
    }

    if (in.readByte() == 0x01) {
      warnings = new ArrayList<>();
      in.readList(warnings, String.class.getClassLoader());
    }
    else {
      warnings = null;
    }
    error = in.readString();
  }

  public List<Card> getCards() {
    if (cards == null) {
      cards = new ArrayList<>();
    }
    return cards;
  }

  public List<String> getWarnings() {
    if (warnings == null) {
      warnings = new ArrayList<>();
    }
    return warnings;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  public boolean hasAnyProblems() {
    return error != null || (warnings != null && !warnings.isEmpty());
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel out, int flags) {
    if (cards == null) {
      out.writeByte((byte)(0x00));
    }
    else {
      out.writeByte((byte)(0x01));
      out.writeList(cards);
    }

    if (warnings == null) {
      out.writeByte((byte)(0x00));
    }
    else {
      out.writeByte((byte)(0x01));
      out.writeList(warnings);
    }
    out.writeString(error);
  }

  @SuppressWarnings("unused")
  public static final Parcelable.Creator<ConversionResult> CREATOR = new Parcelable.Creator<ConversionResult>() {
    @Override
    public ConversionResult createFromParcel(Parcel in) {
      return new ConversionResult(in);
    }

    @Override
    public ConversionResult[] newArray(int size) {
      return new ConversionResult[size];
    }
  };
}
