package com.masarov.spacedrepetition.di;

import com.masarov.spacedrepetition.activities.DeckInfoActivity;
import com.masarov.spacedrepetition.activities.DeckListActivity;
import com.masarov.spacedrepetition.activities.EditDeckActivity;
import com.masarov.spacedrepetition.activities.SessionActivity;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
  void inject(DeckListActivity activity);

  void inject(DeckInfoActivity activity);

  void inject(SessionActivity activity);

  void inject(EditDeckActivity activity);
}

