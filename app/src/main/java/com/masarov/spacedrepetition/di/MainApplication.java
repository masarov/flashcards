package com.masarov.spacedrepetition.di;

import android.app.Application;
import android.support.annotation.VisibleForTesting;


public class MainApplication extends Application {

  private ApplicationComponent applicationComponent;

  @Override
  public void onCreate() {
    super.onCreate();

    applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(new ApplicationModule(this))
            .build();
  }

  public ApplicationComponent getComponent() {
    return applicationComponent;
  }

  @VisibleForTesting
  public void setComponent(ApplicationComponent component) {
    this.applicationComponent = component;
  }
}

