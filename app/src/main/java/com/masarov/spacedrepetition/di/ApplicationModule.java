package com.masarov.spacedrepetition.di;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.masarov.spacedrepetition.storage.CardRepository;
import com.masarov.spacedrepetition.storage.DeckRepository;
import com.masarov.spacedrepetition.storage.ResultRepository;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
public class ApplicationModule {

  public static final String CARD_PREFS = "com.masarov.spacedrepetition.CardSharedPrefs";
  public static final String DECK_PREFS = "com.masarov.spacedrepetition.DeckSharedPrefs";
  public static final String RESULT_PREFS = "com.masarov.spacedrepetition.ResultSharedPrefs";

  private Application application;

  public ApplicationModule(Application application) {
    this.application = application;
  }

  @Singleton
  @Provides
  public Gson provideGson() {
    return new Gson();
  }

  @Singleton
  @Provides
  @Named(CARD_PREFS)
  public SharedPreferences provideCardSharedPreferences() {
    return application.getSharedPreferences(CARD_PREFS, Context.MODE_PRIVATE);
  }

  @Singleton
  @Provides
  @Named(DECK_PREFS)
  public SharedPreferences provideDeckSharedPreferences() {
    return application.getSharedPreferences(DECK_PREFS, Context.MODE_PRIVATE);
  }

  @Singleton
  @Provides
  @Named(RESULT_PREFS)
  public SharedPreferences provideResultSharedPreferences() {
    return application.getSharedPreferences(RESULT_PREFS, Context.MODE_PRIVATE);
  }

  @Singleton
  @Provides
  public CardRepository provideCardRepository(Gson gson, @Named(CARD_PREFS) SharedPreferences preferences) {
    return new CardRepository(preferences, gson);
  }

  @Singleton
  @Provides
  public DeckRepository provideDeckRepository(Gson gson, @Named(DECK_PREFS) SharedPreferences preferences) {
    return new DeckRepository(preferences, gson);
  }

  @Singleton
  @Provides
  public ResultRepository provideResultRepository(Gson gson, @Named(RESULT_PREFS) SharedPreferences preferences) {
    return new ResultRepository(preferences, gson);
  }

}
