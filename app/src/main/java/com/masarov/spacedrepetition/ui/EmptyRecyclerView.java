package com.masarov.spacedrepetition.ui;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;


public class EmptyRecyclerView extends RecyclerView {

  private final AdapterDataObserver observer = new AdapterObserver();

  @Nullable
  private View emptyStateView;

  public EmptyRecyclerView(Context context) {
    super(context);
  }

  public EmptyRecyclerView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public EmptyRecyclerView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  @Override
  public void setAdapter(@Nullable Adapter adapter) {
    final Adapter oldAdapter = getAdapter();
    if (oldAdapter != null) {
      oldAdapter.unregisterAdapterDataObserver(observer);
    }

    super.setAdapter(adapter);
    if (adapter != null) {
      adapter.registerAdapterDataObserver(observer);
    }
  }

  public void setEmptyStateView(@Nullable View emptyStateView) {
    this.emptyStateView = emptyStateView;
    setEmptyViewVisibleIfNeeded();
  }

  private void setEmptyViewVisibleIfNeeded() {
    if (emptyStateView != null) {
      emptyStateView.setVisibility(getAdapter().getItemCount() > 0 ? GONE : VISIBLE);
    }
  }

  private class AdapterObserver extends AdapterDataObserver {
    @Override
    public void onChanged() {
      super.onChanged();
      setEmptyViewVisibleIfNeeded();
    }

    @Override
    public void onItemRangeInserted(int positionStart, int itemCount) {
      super.onItemRangeInserted(positionStart, itemCount);
      setEmptyViewVisibleIfNeeded();
    }

    @Override
    public void onItemRangeRemoved(int positionStart, int itemCount) {
      super.onItemRangeRemoved(positionStart, itemCount);
      setEmptyViewVisibleIfNeeded();
    }
  }

}
