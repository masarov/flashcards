package com.masarov.spacedrepetition.ui;


import android.animation.Animator;
import android.view.View;

public class HideViewOnAnimationEndListener implements Animator.AnimatorListener {

  private View view;

  public HideViewOnAnimationEndListener(View view) {
    this.view = view;
  }

  @Override
  public void onAnimationStart(Animator animation) {
  }

  @Override
  public void onAnimationEnd(Animator animation) {
    view.setVisibility(View.GONE);
  }

  @Override
  public void onAnimationCancel(Animator animation) {

  }

  @Override
  public void onAnimationRepeat(Animator animation) {

  }
}
