package com.masarov.spacedrepetition.ui;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class FlipAnimation extends Animation {
  private Camera camera;

  private View fromView;
  private View toView;

  private float centerX;
  private float centerY;

  public FlipAnimation(View fromView, View toView) {
    this.fromView = fromView;
    this.toView = toView;

    setDuration(500);
    setFillAfter(false);
    setInterpolator(new FastOutSlowInInterpolator());
  }

  @Override
  public void initialize(int width, int height, int parentWidth, int parentHeight) {
    super.initialize(width, height, parentWidth, parentHeight);
    centerX = width / 2;
    centerY = height / 2;
    camera = new Camera();
  }

  @Override
  protected void applyTransformation(float interpolatedTime, Transformation t) {
    final double radians = Math.PI * interpolatedTime;
    float degrees = (float) (180.0 * radians / Math.PI);

    if (interpolatedTime >= 0.5f) {
      degrees -= 180.f;
      fromView.setVisibility(View.GONE);
      toView.setVisibility(View.VISIBLE);
    }

    final Matrix matrix = t.getMatrix();
    camera.save();
    camera.rotateY(degrees);
    camera.getMatrix(matrix);
    camera.restore();
    matrix.preTranslate(-centerX, -centerY);
    matrix.postTranslate(centerX, centerY);
  }
}
