package com.masarov.spacedrepetition.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.masarov.spacedrepetition.R;
import com.masarov.spacedrepetition.di.ApplicationComponent;
import com.masarov.spacedrepetition.di.MainApplication;


public abstract class AbstractActivity extends AppCompatActivity {

  @Bind(R.id.toolbar)
  Toolbar toolbar;

  private ApplicationComponent component;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    component = ((MainApplication)getApplication()).getComponent();
  }

  @Override
  public void onContentChanged() {
    super.onContentChanged();
    initToolbar();
  }

  protected ApplicationComponent getComponent() {
    return component;
  }

  protected void initToolbar() {
    ButterKnife.bind(this);

    setSupportActionBar(toolbar);
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setDisplayShowTitleEnabled(true);
    }
  }

}
