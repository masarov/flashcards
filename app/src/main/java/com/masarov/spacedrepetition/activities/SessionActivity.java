package com.masarov.spacedrepetition.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.masarov.spacedrepetition.R;
import com.masarov.spacedrepetition.fragments.CardFragment;
import com.masarov.spacedrepetition.fragments.OnQuestionAnsweredListener;
import com.masarov.spacedrepetition.fragments.OnSummaryReadyListener;
import com.masarov.spacedrepetition.fragments.SessionProvider;
import com.masarov.spacedrepetition.fragments.SessionSummaryFragment;
import com.masarov.spacedrepetition.model.Card;
import com.masarov.spacedrepetition.model.Deck;
import com.masarov.spacedrepetition.model.Session;
import com.masarov.spacedrepetition.storage.CardRepository;
import com.masarov.spacedrepetition.ui.NotScrollableViewPager;

import javax.inject.Inject;
import java.util.List;

public class SessionActivity extends AbstractActivity implements OnQuestionAnsweredListener, SessionProvider {

  @Bind(R.id.view_pager)
  NotScrollableViewPager pager;

  @Inject
  CardRepository cardRepository;

  private Deck deck;
  private Session session = new Session();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_session);
    ButterKnife.bind(this);
    getComponent().inject(this);

    extractDeckFromExtra();
    initViews();
  }

  private void extractDeckFromExtra() {
    deck = getIntent().getParcelableExtra(Deck.EXTRA);
    if (deck == null) {
      throw new UnsupportedOperationException(getString(R.string.deck_extra_missing));
    }
  }

  private void initViews() {
    List<Card> cards = cardRepository.getShuffle(deck.getId());
    CardSlidePagerAdapter adapter = new CardSlidePagerAdapter(getSupportFragmentManager(), cards);
    pager.setAdapter(adapter);
    pager.addOnPageChangeListener(new OnCardChangeListener(adapter, this));
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public void onQuestionAnswered(boolean correct) {
    addAnswerToSession(correct);
    pager.setCurrentItem(pager.getCurrentItem() + 1, true);
  }

  private void addAnswerToSession(boolean correct) {
    Card card = ((CardSlidePagerAdapter)pager.getAdapter()).getCard(pager.getCurrentItem());
    if (correct) {
      session.getCorrectAnswers().add(card);
    }
    else {
      session.getWrongAnswers().add(card);
    }
  }

  @Override
  public Session provideSession() {
    if (session == null) {
      session = new Session();
    }

    return session;
  }

  private static class CardSlidePagerAdapter extends FragmentStatePagerAdapter {

    private List<Card> cards;
    private OnSummaryReadyListener OnSummaryReadyListener;

    public CardSlidePagerAdapter(FragmentManager fm, List<Card> cards) {
      super(fm);
      this.cards = cards;
    }

    @Override
    public Fragment getItem(int position) {
      if (position >= cards.size()) {
        return createSessionSummaryFragment();
      }

      return createCardFragment(position);
    }

    @NonNull
    private Fragment createSessionSummaryFragment() {
      SessionSummaryFragment fragment = new SessionSummaryFragment();
      OnSummaryReadyListener = fragment;
      return fragment;
    }

    @NonNull
    private Fragment createCardFragment(int position) {
      Fragment fragment = new CardFragment();
      fragment.setArguments(createCardBundle(position));
      return fragment;
    }

    @NonNull
    private Bundle createCardBundle(int position) {
      Bundle bundle = new Bundle();
      bundle.putParcelable(Card.EXTRA, cards.get(position));
      return bundle;
    }

    @Override
    public int getCount() {
      return cards.size() + 1;
    }

    public Card getCard(int position) {
      return cards.get(position);
    }

    public OnSummaryReadyListener getOnSummaryReadyListener() {
      return OnSummaryReadyListener;
    }
  }

  private static class OnCardChangeListener implements ViewPager.OnPageChangeListener {

    private CardSlidePagerAdapter adapter;
    private SessionProvider provider;

    public OnCardChangeListener(CardSlidePagerAdapter adapter, SessionProvider provider) {
      this.adapter = adapter;
      this.provider = provider;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
      if (position != (adapter.getCount() - 1)) {
        return;
      }

      OnSummaryReadyListener listener = adapter.getOnSummaryReadyListener();
      if (listener != null) {
        listener.onSummaryReady(provider.provideSession());
      }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
  }

}
