package com.masarov.spacedrepetition.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.View;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.masarov.spacedrepetition.R;
import com.masarov.spacedrepetition.adapters.DeckAdapter;
import com.masarov.spacedrepetition.storage.DeckRepository;
import com.masarov.spacedrepetition.ui.EmptyRecyclerView;

import javax.inject.Inject;


public class DeckListActivity extends AbstractActivity {

  @Bind(R.id.deck_recycle_view)
  EmptyRecyclerView deckRecycleView;

  @Bind(R.id.empty_state_view)
  View emptyStateView;

  @Inject
  DeckRepository deckRepository;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_deck_list);
    ButterKnife.bind(this);
    getComponent().inject(this);
  }

  @Override
  protected void onResume() {
    super.onResume();
    configureRecyclerView();
  }

  private void configureRecyclerView() {
    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
    deckRecycleView.setHasFixedSize(true);
    deckRecycleView.setLayoutManager(layoutManager);
    deckRecycleView.setAdapter(new DeckAdapter(deckRepository.getAll(), this));
    deckRecycleView.setEmptyStateView(emptyStateView);
  }

  @Override
  protected void initToolbar() {
    super.initToolbar();
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(false);
      getSupportActionBar().setDisplayUseLogoEnabled(true);
    }
  }

  @OnClick(R.id.add_new_deck)
  public void startAddNewDeckActivity() {
    startActivity(new Intent(this, EditDeckActivity.class));
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

}
