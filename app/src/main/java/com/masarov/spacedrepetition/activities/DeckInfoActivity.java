package com.masarov.spacedrepetition.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.masarov.spacedrepetition.R;
import com.masarov.spacedrepetition.model.Card;
import com.masarov.spacedrepetition.model.Deck;
import com.masarov.spacedrepetition.storage.CardRepository;

import javax.inject.Inject;
import java.util.List;

public class DeckInfoActivity extends AbstractActivity {

  public static final int EDIT_REQUEST_CODE = 1;

  @Bind(R.id.collapsing_toolbar)
  CollapsingToolbarLayout collapsingToolbarLayout;

  @Bind(R.id.cards_view)
  TextView cardsView;

  @Inject
  CardRepository cardRepository;

  private Deck deck;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_deck_info);
    ButterKnife.bind(this);
    getComponent().inject(this);

    extractDeckFromExtra();
    initCollapsingToolbar();
    initCardsView();
  }

  private void extractDeckFromExtra() {
    deck = getIntent().getParcelableExtra(Deck.EXTRA);
    if (deck == null) {
      throw new UnsupportedOperationException(getString(R.string.deck_extra_missing));
    }
  }

  private void initCollapsingToolbar() {
    int color = ColorGenerator.MATERIAL.getColor(deck.getName());
    collapsingToolbarLayout.setBackgroundColor(color);
    toolbar.setBackgroundColor(color);
    collapsingToolbarLayout.setTitle(deck.getName());
  }

  private void initCardsView() {
    List<Card> cards = cardRepository.get(deck.getId());
    StringBuilder cardsString = new StringBuilder(cards.size() * 17);
    for (Card card : cards) {
      cardsString.append(card.getQuestion()).append(" - ").append(card.getAnswer()).append("\n");
    }
    cardsView.setText(cardsString.toString());
  }

  @Override
  protected void initToolbar() {
    super.initToolbar();
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
  }

  @OnClick(R.id.start_session_fab)
  public void startSessionActivity() {
    Intent intent = new Intent(this, SessionActivity.class);
    intent.putExtra(Deck.EXTRA, deck);
    startActivity(intent);
  }


  @OnClick(R.id.edit_deck_button)
  public void startEditDeckActivity() {
    Intent intent = new Intent(this, EditDeckActivity.class);
    intent.putExtra(Deck.EXTRA, deck);
    startActivityForResult(intent, EDIT_REQUEST_CODE);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == EDIT_REQUEST_CODE) {
      handleEditResult(resultCode, data);
    }
    else {
      super.onActivityResult(requestCode, resultCode, data);
    }
  }

  private void handleEditResult(int resultCode, Intent data) {
    if (resultCode == EditDeckActivity.DECK_DELETED) {
      finish();
    }

    if (resultCode == EditDeckActivity.DECK_CHANGED) {
      deck = data.getParcelableExtra(Deck.EXTRA);
      initCollapsingToolbar();
    }
  }
}
