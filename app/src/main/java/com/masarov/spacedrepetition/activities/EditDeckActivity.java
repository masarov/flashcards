package com.masarov.spacedrepetition.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.masarov.spacedrepetition.R;
import com.masarov.spacedrepetition.fragments.DownloadResultFragment;
import com.masarov.spacedrepetition.fragments.EditDeckFragment;
import com.masarov.spacedrepetition.fragments.OnDeckChangeListener;
import com.masarov.spacedrepetition.model.Deck;
import com.masarov.spacedrepetition.storage.CardRepository;
import com.masarov.spacedrepetition.storage.DeckRepository;
import com.masarov.spacedrepetition.utils.ConversionResult;
import com.masarov.spacedrepetition.utils.Converter;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import javax.inject.Inject;
import java.io.IOException;


public class EditDeckActivity extends AbstractActivity implements OnDeckChangeListener {

  public static final int DECK_DELETED = 11;
  public static final int DECK_CHANGED = 22;

  private final Runnable hideLoadingRunnable = new Runnable() {
    @Override
    public void run() {
      if (loadingView != null) {
        loadingView.setVisibility(View.GONE);
      }
    }
  };

  @Bind(R.id.loading_view)
  View loadingView;

  @Inject
  DeckRepository deckRepository;

  @Inject
  CardRepository cardRepository;

  private Subscription downloadSubscription;
  private Deck existingDeck;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_edit_deck);
    ButterKnife.bind(this);
    getComponent().inject(this);
    extractExtras();
    startEditFragment();
  }

  private void extractExtras() {
    if (getIntent().hasExtra(Deck.EXTRA)) {
      existingDeck = getIntent().getParcelableExtra(Deck.EXTRA);
    }
  }

  private void startEditFragment() {
    Fragment fragment = new EditDeckFragment();
    fragment.setArguments(createEditBundle());
    getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment).commit();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (downloadSubscription != null && downloadSubscription.isUnsubscribed()) {
      downloadSubscription.unsubscribe();
    }
  }

  @NonNull
  private Bundle createEditBundle() {
    Bundle bundle = new Bundle();
    String url = getIntent().getDataString();
    if (existingDeck != null) {
      bundle.putParcelable(EditDeckFragment.EXISTING_DECK, existingDeck);
    }
    if (url != null) {
      bundle.putString(EditDeckFragment.DECK_URL, url);
    }
    return bundle;
  }

  @Override
  public void onStartDownload(final String deckName, final String deckUrl) {
    hideKeyboard();
    loadingView.setVisibility(View.VISIBLE);
    Observable<String> observable = createRequestObservable(deckUrl);
    downloadSubscription = observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(createSubscriber(deckName, deckUrl));
  }

  private void hideKeyboard() {
    View view = getCurrentFocus();
    if (view != null) {
      view.clearFocus();
      InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
      imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
  }

  @NonNull
  private Observable<String> createRequestObservable(final String deckUrl) {
    return Observable.create(new Observable.OnSubscribe<String>() {
      @Override
      public void call(Subscriber<? super String> subscriber) {
        makeRequest(subscriber, deckUrl);
      }
    });
  }

  private void makeRequest(Subscriber<? super String> subscriber, String deckUrl) {
    OkHttpClient client = new OkHttpClient();
    Request request = new Request.Builder().url(deckUrl).build();

    try {
      Response response = client.newCall(request).execute();
      String body = response.body().string();
      subscriber.onNext(body);
      subscriber.onCompleted();
    }
    catch (IOException e) {
      subscriber.onError(e);
    }
  }

  @NonNull
  private Subscriber<String> createSubscriber(final String deckName, final String deckUrl) {
    return new Subscriber<String>() {
      @Override
      public void onCompleted() {
        downloadSubscription.unsubscribe();
      }

      @Override
      public void onError(Throwable e) {
        handleError(e);
      }

      @Override
      public void onNext(String result) {
        handleSuccessfulResponse(result, deckUrl, deckName);
      }
    };
  }

  private void handleError(Throwable e) {
    ConversionResult result = new ConversionResult();
    result.setError(e.getLocalizedMessage());
    startResultFragment(result);
    loadingView.postDelayed(hideLoadingRunnable, 500);
    downloadSubscription.unsubscribe();
  }

  private void startResultFragment(ConversionResult result) {
    DownloadResultFragment fragment = new DownloadResultFragment();
    Bundle bundle = new Bundle();
    bundle.putParcelable(DownloadResultFragment.RESULT, result);
    fragment.setArguments(bundle);
    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
    loadingView.postDelayed(hideLoadingRunnable, 500);
  }

  private void handleSuccessfulResponse(String responseString, String deckUrl, String deckName) {
    ConversionResult result = Converter.convert(deckUrl, responseString);
    startResultFragment(result);

    if (result.getError() != null || result.getCards().isEmpty()) {
      return;
    }

    if (existingDeck == null) {
      createNewDeck(deckUrl, deckName, result);

    }
    else {
      updateExistingDeck(result);
    }
  }

  private void updateExistingDeck(ConversionResult result) {
    existingDeck.setNumberOfCards(result.getCards().size());
    deckRepository.save(existingDeck);
    cardRepository.save(result.getCards(), existingDeck.getId());
  }

  private void createNewDeck(String deckUrl, String deckName, ConversionResult result) {
    Deck deck = new Deck(System.currentTimeMillis(), deckName, deckUrl, result.getCards().size());
    deckRepository.add(deck);
    cardRepository.save(result.getCards(), deck.getId());
  }

  @Override
  public void onDeleteDeck(long deckId) {
    deckRepository.delete(deckId);
    cardRepository.delete(deckId);
    setResult(DECK_DELETED);
    finish();
  }

  @Override
  public void onChangeDeck(Deck deck) {
    deckRepository.save(deck);
    Intent intent = new Intent();
    intent.putExtra(Deck.EXTRA, deck);
    setResult(DECK_CHANGED, intent);
    finish();
  }

  @Override
  public boolean isDeckNameUnique(String name) {
    return !deckRepository.exists(name);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      handleNavigateUp();
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  private void handleNavigateUp() {
    Intent upIntent = NavUtils.getParentActivityIntent(this);
    if (getIntent().getDataString() != null || NavUtils.shouldUpRecreateTask(this, upIntent)) {
      TaskStackBuilder.create(this).addNextIntentWithParentStack(upIntent).startActivities();
      return;
    }
    NavUtils.navigateUpTo(this, upIntent);
  }
}
