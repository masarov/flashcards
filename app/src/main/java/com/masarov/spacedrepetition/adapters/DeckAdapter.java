package com.masarov.spacedrepetition.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.masarov.spacedrepetition.R;
import com.masarov.spacedrepetition.activities.DeckInfoActivity;
import com.masarov.spacedrepetition.model.Deck;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DeckAdapter extends RecyclerView.Adapter<DeckAdapter.DeckViewHolder> {

  private static final String DATE_TEMPLATE = "dd.MM.yyyy";
  private final TextDrawable.IBuilder builder = TextDrawable.builder().round();

  private List<Deck> decks;
  private Context context;

  public DeckAdapter(List<Deck> decks, Context context) {
    this.decks = decks;
    this.context = context;
  }

  @Override
  public DeckViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    return new DeckViewHolder(LayoutInflater.from(context).inflate(R.layout.view_deck_list_item, parent, false));
  }

  @Override
  public void onBindViewHolder(DeckViewHolder holder, int position) {
    Deck deck = decks.get(position);
    holder.getDeckNameView().setText(deck.getName());
    holder.getDeckInfoView().setText(getCreationDate(deck.getId()));
    holder.getNumberOfCardsView().setText(context.getString(R.string.cards, deck.getNumberOfCards()));
    holder.getDeckLogoView().setImageDrawable(builder.build(decks.get(position).getName().substring(0, 1).toUpperCase(), ColorGenerator.MATERIAL.getColor(decks.get(position).getName())));
    holder.getContainerView().setOnClickListener(new OnDeckClickListener(deck, context));
  }

  @NonNull
  private String getCreationDate(long millis) {
    SimpleDateFormat formatter = new SimpleDateFormat(DATE_TEMPLATE, Locale.getDefault());
    return formatter.format(new Date(millis));
  }

  @Override
  public int getItemCount() {
    return decks.size();
  }

  public static class DeckViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.deck_name)
    TextView deckNameView;

    @Bind(R.id.number_of_cards)
    TextView numberOfCardsView;

    @Bind(R.id.deck_info)
    TextView deckInfoView;

    @Bind(R.id.deck_logo)
    ImageView deckLogoView;

    @Bind(R.id.deck_container)
    View containerView;

    public DeckViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }

    public TextView getDeckNameView() {
      return deckNameView;
    }

    public TextView getNumberOfCardsView() {
      return numberOfCardsView;
    }

    public TextView getDeckInfoView() {
      return deckInfoView;
    }

    public ImageView getDeckLogoView() {
      return deckLogoView;
    }

    public View getContainerView() {
      return containerView;
    }
  }

  private static class OnDeckClickListener implements View.OnClickListener {

    private Context context;
    private Deck deck;

    public OnDeckClickListener(Deck deck, Context context) {
      this.deck = deck;
      this.context = context;
    }

    @Override
    public void onClick(View v) {
      Intent intent = new Intent(context, DeckInfoActivity.class);
      intent.putExtra(Deck.EXTRA, deck);
      context.startActivity(intent);
    }
  }
}
