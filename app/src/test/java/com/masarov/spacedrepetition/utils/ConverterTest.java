package com.masarov.spacedrepetition.utils;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class ConverterTest {

  public static final String OUTPUT_CSV = ".../output=csv";
  public static final String OUTPUT_TSV = ".../output=tsv";

  @Test
  public void testConvertFromCsv() {
    ConversionResult result = Converter.convert(OUTPUT_CSV, "q1, a1\r\nq2,a2\r\nq3,a3");

    assertThat(result.hasAnyProblems(), is(false));
    assertThat(result.getCards().size(), is(3));
    assertThat(result.getWarnings().size(), is(0));
    assertThat(result.getError(), is(nullValue()));
  }

  @Test
  public void testConvertFromTsv() {
    ConversionResult result = Converter.convert(OUTPUT_TSV, "q1\t a1\r\nq2\ta2\r\nq3\ta3");

    assertThat(result.hasAnyProblems(), is(false));
    assertThat(result.getCards().size(), is(3));
    assertThat(result.getWarnings().size(), is(0));
    assertThat(result.getError(), is(nullValue()));
  }

  @Test
  public void testConvertNull() {
    ConversionResult result = Converter.convert(OUTPUT_CSV, null);

    assertThat(result.hasAnyProblems(), is(true));
    assertThat(result.getError(), is(Converter.EMPTY_SPREADSHEET_ERROR));
  }

  @Test
  public void testConvertEmpty() {
    ConversionResult result = Converter.convert(OUTPUT_CSV, "");

    assertThat(result.hasAnyProblems(), is(true));
    assertThat(result.getError(), is(Converter.EMPTY_SPREADSHEET_ERROR));
  }

  @Test
  public void testConvertEmptySpace() {
    ConversionResult result = Converter.convert(OUTPUT_CSV, "       ");

    assertThat(result.hasAnyProblems(), is(true));
    assertThat(result.getError(), is(Converter.EMPTY_SPREADSHEET_ERROR));
  }

  @Test
  public void testConvertNotCsvOrTsv() {
    ConversionResult result = Converter.convert(OUTPUT_CSV, "<!DOCTYPE html><html lang=");

    assertThat(result.hasAnyProblems(), is(true));
    assertThat(result.getError(), is(Converter.WRONG_OUTPUT_FORMAT));
  }

  @Test
  public void testConvertMalformedCsvWithNoCommas() {
    ConversionResult result = Converter.convert(OUTPUT_CSV, " q1 a1");

    assertThat(result.hasAnyProblems(), is(true));
    assertThat(result.getWarnings().size(), is(1));
    assertThat(result.getWarnings().get(0), is("[ q1 a1]"));
    assertThat(result.getCards().size(), is(0));
  }

  @Test
  public void testConvertMalformedTsvWithNoTabs() {
    ConversionResult result = Converter.convert(OUTPUT_TSV, " q1 a1");

    assertThat(result.hasAnyProblems(), is(true));
    assertThat(result.getWarnings().size(), is(1));
    assertThat(result.getWarnings().get(0), is("[ q1 a1]"));
    assertThat(result.getCards().size(), is(0));
  }

  @Test
  public void testConvertCsvWithOneMalformedCsvEntry() {
    ConversionResult result = Converter.convert(OUTPUT_CSV, "q1, a1\r\nq2, ,,a2\r\nq3,a3");

    assertThat(result.hasAnyProblems(), is(true));
    assertThat(result.getWarnings().size(), is(1));
    assertThat(result.getWarnings().get(0), is("[q2,  , , a2]"));
    assertThat(result.getCards().size(), is(2));
  }

  @Test
  public void testConvertCsvWithOneMalformedTsvEntry() {
    ConversionResult result = Converter.convert(OUTPUT_TSV, "q1\t a1\r\nq2\t \t\ta2\r\nq3\ta3");

    assertThat(result.hasAnyProblems(), is(true));
    assertThat(result.getWarnings().size(), is(1));
    assertThat(result.getWarnings().get(0), is("[q2,  , , a2]"));
    assertThat(result.getCards().size(), is(2));
  }

}